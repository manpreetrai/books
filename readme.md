|S. No|Book Name                                                                         |Author                                |Recommended|Category|Notes|PDF|EPUB|TXT|
|-----|----------------------------------------------------------------------------------|--------------------------------------|-----------|--------|-----|---|----|---|
|1    |Sri Guru Granth Sahib Ji                                                          |Infinite                              |P          |        |     |Y  |    |   |
|2    |Srimadh Bhagvat Geeta                                                             |Infinite                              |P          |        |     |Y  |    |   |
|3    |The Brothers Karamazov By Constance Clara Garnett Or Unabridged                   |Fyodor Dostoevsky                     |P          |        |     |Y  |    |   |
|4    |Atomic Habits                                                                     |James Clear                           |29         |        |     |Y  |    |   |
|5    |The Alchemist                                                                     |Paulo Coelho                          |6          |        |     |Y  |    |   |
|6    |The Psychology Of Money                                                           |Morgan Housel                         |18         |        |     |Y  |    |   |
|7    |Rich Dad Poor Dad                                                                 |Robert T. Kiyosaki                    |6          |        |     |Y  |    |   |
|8    |Deep Work                                                                         |Cal Newport                           |11         |        |     |Y  |    |   |
|9    |Power                                                                             |Robert Greene                         |2          |        |     |Y  |    |   |
|10   |Life Is Long If You Know How To Use It: On The Shortness Of Life                  |Seneca                                |2          |        |     |Y  |    |   |
|11   |The Almanack Of Naval Ravikant                                                    |Eric Jorgenson                        |12         |        |     |Y  |    |   |
|12   |The Subtle Art Of Not Giving A Fuck                                               |Mark Manson                           |13         |        |     |Y  |    |   |
|13   |Think Again                                                                       |Adam Grant                            |5          |        |     |Y  |    |   |
|14   |Meditations                                                                       |Marcus Aurelius                       |3          |        |     |Y  |    |   |
|15   |Why We Sleep                                                                      |Matthew Walker                        |3          |        |     |Y  |    |   |
|16   |Hyper Focus                                                                       |Bailey                                |5          |        |     |Y  |    |   |
|17   |It Always Seems Impossible Until It'S Done                                        |Kathryn & Ross Petras                 |1          |        |     |Y  |    |   |
|18   |1984                                                                              |George Orwell                         |P          |        |     |Y  |    |   |
|19   |Bad Blood                                                                         |John Carreyrou                        |P          |        |     |Y  |    |   |
|20   |Flow                                                                              |Mihaly                                |1          |        |     |Y  |    |   |
|21   |Never Split The Difference                                                        |Chris Voss                            |5          |        |     |Y  |    |   |
|22   |Sapiens: A Brief History Of Humankind                                             |Yuval Noah Harari                     |6          |        |     |Y  |    |   |
|23   |The Denial Of Death                                                               |Earnest Becker                        |1          |        |     |Y  |    |   |
|24   |Can'T Hurt Me                                                                     |David Goggins                         |7          |        |     |Y  |    |   |
|25   |The Evolution Of God                                                              |Robert Wright                         |1          |        |     |Y  |    |   |
|26   |The Laws Of Human Nature                                                          |Robert Greene                         |3          |        |     |Y  |    |   |
|27   |Obedience To Authority                                                            |Stanley Milgram                       |1          |        |     |Y  |    |   |
|28   |God Against The Gods                                                              |Jonathan Kirsch                       |1          |        |     |Y  |    |   |
|29   |Why We Buy                                                                        |Paco Underhill                        |1          |        |     |   |    |   |
|30   |The 22 Immutable Laws Of Marketing                                                |Al Ries And Jack Trout                |1          |        |     |   |    |   |
|31   |Beyond Order 12 More Rules Of Life                                                |Jordan B Peterson                     |2          |        |     |   |    |   |
|32   |The 7 Habits Of Highly Effective People                                           |Stephen R Covey                       |4          |        |     |   |    |   |
|33   |Autobiography Of A Yogi                                                           |Paramhansa Yogananda                  |1          |        |     |   |    |   |
|34   |A Brief History Of Time From The Big Bang To Black Holes                          |Stephen Hawking                       |1          |        |     |   |    |   |
|35   |How To Win Friends And Influence People                                           |Dale Carnegie                         |10         |        |     |   |    |   |
|36   |Tuesdays With Morrie: An Old Man, A Young Man, And Life'S Greatest Lesson         |Mitch Albom                           |3          |        |     |   |    |   |
|37   |Man'S Search For Meaning                                                          |Viktor E. Frankl                      |9          |        |     |   |    |   |
|38   |The Art And Science Of Remembering Everything: Moonwalking With Einstein          |Joshua Foer                           |1          |        |     |   |    |   |
|39   |Where Good Ideas Come From: The Seven Patterns Of Innovation                      |Steven Johnson                        |1          |        |     |   |    |   |
|40   |The Book Of Why: The New Science Of Cause And Effect                              |And Dana Mackenzie                    |1          |        |     |   |    |   |
|41   |Until The End Of Time                                                             |Brian Greene                          |1          |        |     |   |    |   |
|42   |Other Minds                                                                       |Peter Godfrey Smith                   |1          |        |     |   |    |   |
|43   |Chimpanzee Politics: Power And Sex Among Apes                                     |Frans De Waal                         |1          |        |     |   |    |   |
|44   |The Monk Who Sold Ferrari                                                         |Robin Sharma                          |2          |        |     |   |    |   |
|45   |The Power Of Now: A Guide To Spiritual Enlightenment                              |Eckhart Tolle                         |2          |        |     |   |    |   |
|46   |Think And Grow Rich                                                               |Napoleon Hill                         |4          |        |     |   |    |   |
|47   |The Greatest Salesman In The World                                                |Og Mandino                            |1          |        |     |   |    |   |
|48   |A Useful Reading Guide                                                            |Shiromani Kant                        |1          |        |     |   |    |   |
|49   |Do The Work                                                                       |Steven Pressfield                     |1          |        |     |   |    |   |
|50   |Anything You Want: 40 Lessons For A New Kind Of Enterpreneur                      |Derek Sivers                          |2          |        |     |   |    |   |
|51   |The Four Agreements                                                               |Don Miguel Ruiz                       |1          |        |     |   |    |   |
|52   |High Performance Habits                                                           |Brendon Burchard                      |3          |        |     |   |    |   |
|53   |The New Rules Of Business                                                         |Rajesh Srivastava                     |1          |        |     |   |    |   |
|54   |How To Have Confidence And Power In Dealing With People                           |Les Giblin                            |1          |        |     |   |    |   |
|55   |Eating In The Age Of Dieting                                                      |Rujuta Diwekar                        |1          |        |     |   |    |   |
|56   |The 5 AM Club                                                                     |Robin Sharma                          |5          |        |     |   |    |   |
|57   |How I Raised Myself From Failure To Success In Selling                            |Frank Bettger                         |1          |        |     |   |    |   |
|58   |Purple Cow: Transform Your Business By Being Remarkable                           |Seth Godin                            |1          |        |     |   |    |   |
|59   |Youtube Secrets                                                                   |Sean Cannell And Benji Travis         |1          |        |     |   |    |   |
|60   |Copyrighting Secrets                                                              |Jim Edwards                           |1          |        |     |   |    |   |
|61   |The Real Estate Wholesaling Bible                                                 |Than Merrill                          |1          |        |     |   |    |   |
|62   |Triggers                                                                          |Joseph Sugerman                       |1          |        |     |   |    |   |
|63   |Indistractable: How To Control Your Attention And Choose Your Life                |Nir Eyal                              |7          |        |     |   |    |   |
|64   |The Compound Effect                                                               |Darren Hardy                          |13         |        |     |   |    |   |
|65   |How To Talk To Anyone                                                             |Leil Lowndes                          |3          |        |     |   |    |   |
|66   |Ikigai                                                                            |Hector Garcia And Francesc Miralles   |11         |        |     |   |    |   |
|67   |The Magic Of Thinking Big                                                         |David Schwartz                        |4          |        |     |   |    |   |
|68   |Think Like A Monk                                                                 |Jay Shetty                            |2          |        |     |Y  |    |   |
|69   |Getting Things Done: The Art Of Stress-Free Productivity                          |David Allen                           |2          |        |     |   |    |   |
|70   |Mindset                                                                           |Dr Carol S. Dweck                     |6          |        |     |   |    |   |
|71   |Make Your Bed                                                                     |William H. Mcraven                    |5          |        |     |   |    |   |
|72   |Extreme Ownership: How Us Navy Seals Lead And Win                                 |Jocko Willink And Leif Babin          |4          |        |     |   |    |   |
|73   |Peak Performance                                                                  |Brad Stulberg And Steve Magness       |1          |        |     |   |    |   |
|74   |Grit: The Power Of Passion And Perseverance                                       |Angela Duckworth                      |4          |        |     |   |    |   |
|75   |Ego Is The Enemy                                                                  |Ryan Holiday                          |4          |        |     |   |    |   |
|76   |The 5 Second Rule                                                                 |Mel Robbins                           |1          |        |     |Y  |    |   |
|77   |Learn To Earn                                                                     |Peter Lynch And John Rothchild        |2          |        |     |   |    |   |
|78   |One Up On Wall Street                                                             |Peter Lynch And John Rothchild        |2          |        |     |   |    |   |
|79   |Coffee Can Investing                                                              |Saurabh Mukherjea                     |1          |        |     |   |    |   |
|80   |The Intelligent Investor                                                          |Benjamin Graham                       |1          |        |     |   |    |   |
|81   |High Probabilty Trading                                                           |Marcel Link                           |1          |        |     |   |    |   |
|82   |Zero To One                                                                       |Peter Thiel And Blake Masters         |2          |        |     |   |    |   |
|83   |The Millionaire Fastlane                                                          |Mj Demarco                            |1          |        |     |   |    |   |
|84   |The One Thing                                                                     |Gary Keller And Jay Papasan           |10         |        |     |   |    |   |
|85   |Influence: The Psychology Of Persuasion                                           |Robert B. Cialdini                    |4          |        |     |   |    |   |
|86   |The Ride Of A Lifetime: Lessons In Creative Leadership                            |Robert Iger                           |1          |        |     |   |    |   |
|87   |Secrets Of Divine Love                                                            |A. Helwa                              |1          |        |     |   |    |   |
|88   |Who Says You Can'T ? You Do                                                       |Daniel Chidiac                        |1          |        |     |   |    |   |
|89   |Success Through A Positive Mental Attitude                                        |Napoleon Hill And W. Clement Stone    |1          |        |     |   |    |   |
|90   |Barking Up The Wrong Tree                                                         |Eric Barker                           |1          |        |     |   |    |   |
|91   |The Obstacle Is The Way                                                           |Ryan Holiday                          |1          |        |     |   |    |   |
|92   |Shoe Dog: A Memoir By The Creator Of Nike                                         |Phil Knight                           |3          |        |     |   |    |   |
|93   |Everything Is Fucked: A Book About Hope                                           |Mark Manson                           |2          |        |     |   |    |   |
|94   |Essentialism: The Disciplined Pursuit Of Less                                     |Greg Mckeown                          |9          |        |     |   |    |   |
|95   |The Rudest Book Ever                                                              |Shwetabh Gangwar                      |2          |        |     |   |    |   |
|96   |Your Money Or Your Life                                                           |Vicki Robin                           |2          |        |     |   |    |   |
|97   |Good Vibes, Good Life                                                             |Vex King                              |1          |        |     |   |    |   |
|98   |Talk Like Ted                                                                     |Carmine Gallo                         |1          |        |     |   |    |   |
|99   |The 80 20 Principle                                                               |Richard Koch                          |1          |        |     |   |    |   |
|100  |Rework: Change The Way You Work Forever                                           |David Heinemeier Hansson              |1          |        |     |   |    |   |
|101  |The Strangest Secret: The Original Version                                        |Earl Nightingale                      |2          |        |     |   |    |   |
|102  |As A Man Thinketh                                                                 |James Allen                           |3          |        |     |   |    |   |
|103  |12 Rules For Life: An Antidote To Chaos                                           |Jordan B. Peterson                    |5          |        |     |   |    |   |
|104  |Thinking, Fast And Slow                                                           |Daniel Kahneman                       |3          |        |     |   |    |   |
|105  |Start With Why                                                                    |Simon Sinek                           |2          |        |     |   |    |   |
|106  |Find Your Red Thread                                                              |Tamsen Webster                        |1          |        |     |   |    |   |
|107  |The Four Year Career                                                              |Richard Bliss Brooke                  |1          |        |     |   |    |   |
|108  |Pineapple Podcasting                                                              |Samantha Lee Wright                   |1          |        |     |   |    |   |
|109  |The Back Door                                                                     |Ishan Goel                            |1          |        |     |   |    |   |
|110  |Side Adventure                                                                    |Artin Nazarian                        |1          |        |     |   |    |   |
|111  |Sign & Thrive                                                                     |Bill Soroka                           |1          |        |     |   |    |   |
|112  |Your Next Five Moves                                                              |Patrick Bet-David And Greg Dinkin     |2          |        |     |   |    |   |
|113  |So Good They Can'T Ignore You                                                     |Cal Newport                           |1          |        |     |   |    |   |
|114  |I Will Teach You To Be Rich                                                       |Ramit Sethi                           |2          |        |     |   |    |   |
|115  |$100M Offers                                                                      |Alex Hormozi                          |2          |        |     |   |    |   |
|116  |The Science Of Getting Rich                                                       |Wallace D. Wattles                    |2          |        |     |   |    |   |
|117  |The Code Of The Extraordinary Mind                                                |Vishen Lakhiani                       |1          |        |     |   |    |   |
|118  |Limitless                                                                         |Jim Kwik                              |1          |        |     |   |    |   |
|119  |How Not To Die                                                                    |Michael Greger                        |1          |        |     |   |    |   |
|120  |The Science And Technology Of Growing Young                                       |Sergey Young                          |1          |        |     |   |    |   |
|121  |Breath                                                                            |James Nestor                          |1          |        |     |   |    |   |
|122  |Lifespan                                                                          |Dacid A. Sinclair                     |2          |        |     |   |    |   |
|123  |The Extended Mind                                                                 |Annie Murphy Paul                     |1          |        |     |   |    |   |
|124  |Useful Delusions                                                                  |Shankar Vedantam                      |1          |        |     |   |    |   |
|125  |The Hidden Spring                                                                 |Mark Solms                            |1          |        |     |   |    |   |
|126  |The 10X Rule                                                                      |Grant Cardone                         |1          |        |     |   |    |   |
|127  |Who Will Cry When You Die?                                                        |Robin Sharma                          |1          |        |     |   |    |   |
|128  |Subliminal: How Your Unconscious Mind Rules Your Behaviour                        |Leonard Mlodinow                      |1          |        |     |   |    |   |
|129  |The Power Of Your Subconscious Mind                                               |Joseph Murphy                         |5          |        |     |   |    |   |
|130  |Pre-Suasion                                                                       |Robert Cialdini                       |1          |        |     |   |    |   |
|131  |The Elephant In The Brain                                                         |Kevin Simler And Robin Hawson         |1          |        |     |   |    |   |
|132  |Wanting: The Power Of Mimetic Desire In Everyday Life                             |Luke Burgis                           |1          |        |     |   |    |   |
|133  |Discipline Equals Freedom                                                         |Jocko Wilink                          |2          |        |     |   |    |   |
|134  |The Dip                                                                           |Seth Godin                            |1          |        |     |   |    |   |
|135  |The Third Door                                                                    |Alex Banana                           |1          |        |     |   |    |   |
|136  |Lying                                                                             |Sam Harris                            |1          |        |     |   |    |   |
|137  |The Daily Stoic                                                                   |Ryan Holiday                          |1          |        |     |   |    |   |
|138  |Steve Jobs                                                                        |Walter Isaacson                       |1          |        |     |   |    |   |
|139  |A Calandar Of Wisdom                                                              |Leo Tolstoy                           |1          |        |     |   |    |   |
|140  |The Millionaire Next Door                                                         |Thomas J. Stanley And William B. Dunko|2          |        |     |   |    |   |
|141  |Factfulness                                                                       |Hans Rosling                          |4          |        |     |   |    |   |
|142  |The Lessons Of History                                                            |Will & Ariel Durant                   |1          |        |     |   |    |   |
|143  |War And Peace                                                                     |Leo Tolstoy                           |1          |        |     |   |    |   |
|144  |Homo Deus: A Brief History Of Tomorrow                                            |Yuval Noah Harari                     |3          |        |     |   |    |   |
|145  |21 Lessons For The 21Th Century                                                   |Yuval Noah Harari                     |2          |        |     |   |    |   |
|146  |The Power Of Habit                                                                |Charles Duhigg                        |2          |        |     |   |    |   |
|147  |A Short Story Of Nearly Everything                                                |Bill Bryson                           |1          |        |     |   |    |   |
|148  |Einstein: His Life And Universe                                                   |Walter Isaacson                       |1          |        |     |   |    |   |
|149  |Leonardo Davinci: The Biography                                                   |Walter Isaacson                       |1          |        |     |   |    |   |
|150  |Benjamin Franklin: An American Life                                               |Walter Isaacson                       |1          |        |     |   |    |   |
|151  |Built To Serve                                                                    |Evan Carmichael                       |1          |        |     |   |    |   |
|152  |The Go-Giver                                                                      |Bob Burg And John David Mann          |1          |        |     |   |    |   |
|153  |Lives Of The Stoics                                                               |Ryan Holiday                          |1          |        |     |   |    |   |
|154  |Stillness Is The Key                                                              |Ryan Holiday                          |1          |        |     |   |    |   |
|155  |The Rational Optimist                                                             |Matt Ridley                           |1          |        |     |   |    |   |
|156  |Who Moved My Cheese?                                                              |Spencer Johnson                       |3          |        |     |   |    |   |
|157  |Crime And Punishment                                                              |Fyodor Dostoevsky By Michael R. Katz  |1          |        |     |   |    |   |
|158  |Measure What Matters                                                              |John Doerr                            |1          |        |     |   |    |   |
|159  |The Everything Store                                                              |Brad Stone                            |1          |        |     |   |    |   |
|160  |The Richest Man In Babylon                                                        |George S. Clason                      |3          |        |     |   |    |   |
|161  |Kafka On The Shore                                                                |Murakami                              |1          |        |     |   |    |   |
|162  |Educated: A Memoir                                                                |Tara Westover                         |1          |        |     |   |    |   |
|163  |Beyond Good And Evil                                                              |Friedrich Neitzsche                   |1          |        |     |   |    |   |
|164  |The Millionair Fastlane                                                           |Mj Demarco                            |2          |        |     |   |    |   |
|165  |The Lean Startup                                                                  |Eric Ries                             |1          |        |     |   |    |   |
|166  |Elon Musk                                                                         |Ashlee Vance                          |3          |        |     |   |    |   |
|167  |Tesla                                                                             |W. Bernard Carlson                    |1          |        |     |   |    |   |
|168  |The Black Swan                                                                    |Nassim Nicholas Taleb                 |1          |        |     |   |    |   |
|169  |The Story Of Philosophy                                                           |Will Durant                           |1          |        |     |   |    |   |
|170  |Principles                                                                        |Ray Dalio                             |2          |        |     |   |    |   |
|171  |The Hard Thing About Hard Things                                                  |Ben Horowitz                          |2          |        |     |   |    |   |
|172  |The Art Of War                                                                    |Sun Tzu By Thomas Cleary              |1          |        |     |   |    |   |
|173  |Fooled By Randomness                                                              |Nassim Nicholas Taleb                 |1          |        |     |   |    |   |
|174  |Leaders Eat Last                                                                  |Simon Sinek                           |1          |        |     |   |    |   |
|175  |Outliers                                                                          |Malcolm Gladwell                      |2          |        |     |   |    |   |
|176  |When Beath Becomes Air                                                            |Paul Kalanith                         |1          |        |     |   |    |   |
|177  |Money: Master The Game                                                            |Tony Robbins                          |2          |        |     |   |    |   |
|178  |The 4-Hour Workweek                                                               |Timothy Ferriss                       |3          |        |     |   |    |   |
|179  |All Marketers Tell Stories                                                        |Seth Godin                            |1          |        |     |   |    |   |
|180  |Reality Is Not What It Seems                                                      |Carlo Rovelli                         |1          |        |     |   |    |   |
|181  |Skin In The Game                                                                  |Nassim Nicholas Taleb                 |2          |        |     |   |    |   |
|182  |Blink                                                                             |Malcolm Gladwell                      |1          |        |     |   |    |   |
|183  |The Great Gatsby                                                                  |F. Scott Fitzgerrald                  |1          |        |     |   |    |   |
|184  |Siddhartha                                                                        |Hermann Hesse                         |1          |        |     |   |    |   |
|185  |Digital Minimalism                                                                |Cal Newport                           |1          |        |     |   |    |   |
|186  |The War Of Art                                                                    |Steven Pressfield                     |2          |        |     |   |    |   |
|187  |Letters From A Stoic                                                              |Seneca                                |1          |        |     |   |    |   |
|188  |The Innovator'S Dilemma                                                           |Clayton M. Christensen                |1          |        |     |   |    |   |
|189  |Hooked                                                                            |Nir Eyal                              |1          |        |     |   |    |   |
|190  |The Fault In Our Stars                                                            |John Green                            |1          |        |     |   |    |   |
|191  |The Art Of Thinking Clearly                                                       |Rolf Dobelli                          |1          |        |     |   |    |   |
|192  |The Emperor'S Handbook - Meditations New Translation By C. Scot And David V. Hicks|Marcus Aurelius                       |1          |        |     |   |    |   |
|193  |The Simple Path To Wealth                                                         |Jl Collins                            |1          |        |     |   |    |   |
|194  |The Basics Of Bitcoins And Blockchains                                            |Anthony Lewis                         |1          |        |     |   |    |   |
|195  |This Bright Future                                                                |Bobby Hill                            |1          |        |     |   |    |   |
|196  |Eat That Frog!                                                                    |Brian Tracy                           |5          |        |     |   |    |   |
|197  |The Miracle Morning                                                               |Hal Elrod                             |3          |        |     |   |    |   |
|198  |Freedom From Known                                                                |J Krishnamurti                        |1          |        |     |   |    |   |
|199  |The Rational Male                                                                 |Rollo Tomassi                         |1          |        |     |   |    |   |
|200  |The Man'S Guide To Women                                                          |John Gottman And Julie Schwartz       |1          |        |     |   |    |   |
|201  |What Women Want                                                                   |Tucker Max And Geoffrey Miller        |1          |        |     |   |    |   |
|202  |No More Mr.Nice Guy                                                               |Robert A. Glover                      |1          |        |     |   |    |   |
|203  |How To Not Die Alone                                                              |Logan Ury                             |1          |        |     |   |    |   |
|204  |Anti-Fragile                                                                      |Nassim Nicholas Taleb                 |1          |        |     |   |    |   |
|205  |The Shit They Never Taught You.                                                   |Adam Jones & Adam Ashton              |1          |        |     |   |    |   |
|206  |Creative Capitalism                                                               |Michael Kinsley                       |1          |        |     |   |    |   |
|207  |Die With Zero                                                                     |Bill Perkins                          |1          |        |     |   |    |   |
|208  |Selfie                                                                            |Will Storr                            |1          |        |     |   |    |   |
|209  |Black Box Thinking                                                                |Matthew Syed                          |1          |        |     |   |    |   |
|210  |Money Land                                                                        |Oliver Bullough                       |1          |        |     |   |    |   |
|211  |Dotcom Secrets                                                                    |Russel Brunson                        |1          |        |     |   |    |   |
|212  |The E Myth Revisited                                                              |Michael E. Gerber                     |1          |        |     |   |    |   |
|213  |Crushing It!                                                                      |Gary Vaynerchuk                       |1          |        |     |   |    |   |
|214  |Holy Shit We'Re Alive                                                             |Doug Cartwright                       |2          |        |     |   |    |   |
|215  |Mind Management Not Time Management                                               |David Kadavy                          |1          |        |     |   |    |   |
|216  |101 Essays That Will Change The Way You Think                                     |Brianna Wiest                         |1          |        |     |   |    |   |
|217  |The Seven Spiritual Laws Of Success                                               |Deepak Chopra                         |1          |        |     |   |    |   |
|218  |The Talent Code                                                                   |Daniel Coyle                          |1          |        |     |   |    |   |
|219  |The Six Pillars Of Self-Esteem                                                    |Nathaniel Branden                     |2          |        |     |   |    |   |
|220  |Top Dog: The Science Of Winning And Losing                                        |Po Bronson And Ashley Merryman        |1          |        |     |   |    |   |
|221  |Presence                                                                          |Amy Cuddy                             |1          |        |     |   |    |   |
|222  |Intuition                                                                         |Allegra Goodman                       |1          |        |     |   |    |   |
|223  |This Is Marketing                                                                 |Seth Godin                            |1          |        |     |   |    |   |
|224  |Good To Great                                                                     |Jim Collins                           |1          |        |     |   |    |   |
|225  |Six Easy Pieces                                                                   |Richard P. Feynman                    |1          |        |     |   |    |   |
|226  |Superintelligence                                                                 |Nick Bostrom                          |1          |        |     |   |    |   |
|227  |Projections                                                                       |Karl Deisseroth                       |1          |        |     |   |    |   |
|228  |A Minute To Think                                                                 |Juliet Funt                           |1          |        |     |   |    |   |
|229  |I Am Malala                                                                       |Malala Yousafzai                      |1          |        |     |   |    |   |
|230  |Why A Students Work For C Students                                                |Robert T. Kiyosaki                    |1          |        |     |   |    |   |
|231  |The Denial Of Death                                                               |Ernest Becker                         |1          |        |     |   |    |   |
|232  |The Rightous Mind                                                                 |Jonathan Haidt                        |1          |        |     |   |    |   |
|233  |Why Buddhism Is True                                                              |Robert Wright                         |1          |        |     |   |    |   |
|234  |Stumbling On Happiness                                                            |Daniel Gilbert                        |1          |        |     |   |    |   |
|235  |You Can Heal Your Life                                                            |Louise Hay                            |1          |        |     |   |    |   |
|236  |Quite                                                                             |Susan Cain                            |1          |        |     |   |    |   |
|237  |Willpower                                                                         |Roy F. Baumeister And John Tierney    |1          |        |     |   |    |   |
|238  |Living With A Seal                                                                |Jesse Itzler                          |1          |        |     |   |    |   |
|239  |The Willpower Instinct                                                            |Kelly McGonigal                       |1          |        |     |   |    |   |
|240  |Attitude Everything                                                               |Jeef Keller                           |1          |        |     |   |    |   |
|241  |You Are A Badass                                                                  |Jen Sincero                           |1          |        |     |   |    |   |
|242  |The Man Who Solved The Market                                                     |Gregory Zuckerman                     |1          |        |     |   |    |   |
|243  |Masters Of Scale                                                                  |Reid Hoffman                          |1          |        |     |   |    |   |
|244  |Startup Myths And Models                                                          |Rizwan Virk                           |1          |        |     |   |    |   |
|245  |The $100 Startup                                                                  |Chris Guillebeau                      |1          |        |     |   |    |   |
|246  |Do It Today                                                                       |Dakius Foroux                         |1          |        |     |   |    |   |
|247  |The Lazy Genius Way                                                               |Kendra Adachi                         |1          |        |     |   |    |   |
